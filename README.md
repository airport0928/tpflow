<div align="center">
<br/>
<br/>
  <h1 align="center">
    Tpflow 工作流引擎 v6.0
  </h1>
  <h4 align="center">
    致 力 于 企 业 信 息 化 解 决 方 案
  </h4> 

[预 览](http://tpflow.gadmin8.com)   |   [官 网](http://www.gadmin8.com/)   |   [群聊](https://jq.qq.com/?_wv=1027&k=uIJZE54F) |   [文档](https://gadmin8.com/index/product.html) |   [Gadmin产品](http://gadmin8.com)
</div>
<div align="center">

[![GitHub stars](https://img.shields.io/badge/license-Mit-yellowgreen?style=flat-square&logo=github)](https://gitee.com/ntdgg/tpflow)
[![GitHub forks](https://img.shields.io/badge/Tpflow-6.0-brightgreen?style=flat-square&logo=github)](https://gitee.com/ntdgg/tpflow)
[![GitHub license](https://img.shields.io/badge/Language-PHP8-orange?style=flat-square&logo=)](https://gitee.com/ntdgg/tpflow)

</div>

* TpFlow工作流引擎是一套规范化的流程管理系统，基于业务而驱动系统生命力的一套引擎。
* 彻底释放整个信息管理系统的的活力，让系统更具可用性，智能应用型，便捷设计性。
* Tpflow团队致力于打造中国最优秀的PHP工作流引擎。

## 😍 奖励计划
Tpflow千元开源奖励计划：https://gitee.com/ntdgg/tpflow/issues/I54XQ6


推荐阿里云服务器 [传送门](https://www.aliyun.com/minisite/goods?userCode=6y4qge6i "Demo")

Gadmin企业级开发平台：  [传送门](https://gadmin8.com "Demo") 包含在线演示模块 测试账号：test 123456

## ☀️青年计划
团队一直致力于中国PHP工作流引擎、业务表单引擎的研发和设计，至今已经有4个年头，2018年初正式立项研发工作流引擎，2019年获得开源中国GVP项目。TPFLOW、SFDP全部使用最宽松的开源MIT协议（可以应用于商业系统、个人系统均可，只需保留版权信息）;[使用声明](https://www.gadmin8.com/index/doc/show.html?id=9&pid=177)

青年计划，为了PHP开源工作流引擎、业务表单引擎的进步，我们全面开启青年计划，服务更多青年学者，为开源做奉献，为工作流引擎做奉献！[前去申请](https://gadmin8.com/index/young.html)


## ⭐快速体验指南
code-galaxy一键快速部署：[传送门](https://console.code-galaxy.net/#/register?invite=hzwVOM "传送门") 注册后即可再应用市场一键安装Tpflow引擎

宝塔一键安装指南：[传送门](https://www.gadmin8.com/index/doc/show.html?id=9&pid=185 "传送门")

## ⭐交流群

交流群①：532797225  （已满）

交流群②：778657302

Vip交流群：1062040103  加群条件：[申请加群](https://www.cojz8.com/article/148 "加群条件")

手册：https://gadmin8.com/index/product.html  【付费】

## ❤  [流之云科技](https://liuzhiyun.com) —— 旗下作品

| 名称                                         | 说明             |
  | -------------------------------------------- | ---------------- |
| [Tpflow](https://gitee.com/ntdgg/tpflow)     | PHP 工作流引擎   |
| [SFDP](https://gitee.com/ntdgg/sfdp)         | PHP 超级表单     |
| [Fkreport](https://gitee.com/ntdgg/Fkreport) | PHP 报表开发平台 |
| [Gadmin](https://gadmin8.com)                | 企业级开发平台   |
| [Gwork](https://liuzhiyun.com/)              | 企业级微办公管理平台   |

## ✈️ 技术架构图&产品截图

<p align="center">
<img src="https://www.gadmin8.com/img/tpflow_er.jpg"  />
</p>

<p align="center">
<img src="https://www.gadmin8.com/img/tpflow_1.png"  />
</p>
<p align="center">
<img src="https://www.gadmin8.com/img/tpflow_2.png"  />
</p>
# ✈️主要特性


## ♨️6.0 新增得特性功能

*   基于`<AntV X6>` 新版图形引擎，让流程设计更加专业
    * 步骤可视化拖动设计
    * 消息步骤 处理消息逻辑实务，知晓业务等
    * 逻辑步骤 支持多线处理步骤


*  `<Auto>` 自动化执行
    * 根据业务逻辑可自动化执行步骤信息


*  全新属性设计界面  `步骤更清晰` `设计更简单`
    * 取消无意义得设置项

*  废弃事务模型
    * 事务SQL在6.0版本后正式取消，可采用事件处理

+ 完善的流引擎机制
    + 规范的命名空间，可拓展的集成化开发
    + 支持 直线式、会签式、转出式、同步审批式等多格式的工作流格式
    + 支持自定义事务驱动
    + 支持各种ORM接口
    + 业务驱动接口
+ 提供基于 `Thinkphp6.0.X` 的样例Demo
+ 提供完整的设计手册
+ 支持PHP8.0


## ♨️5.0 有的新特性及功能

*   基于`<Entrust>`驱动的代理模式管理模块
    * 可以随心调用工作流管理模式
    * 可以代理工作流的审核审批人员
*  `<LoadClass>` 支持自定义的业务驱动模式
    * 业务办理前，办理后的的各种业务流程处理
*  全新的工作流设计界面  `步骤更清晰` `设计更简单`
    * 独立化步骤显示
    * TAB式步骤属性配置
    * 步骤审批、步骤模式更加清晰
*  环形审批流模式
    * 解决以往A发起人->B审核人->C核准人->A发起人完结 的环型审批流

## ⏰安装使用教程
*  安装Composer
*  composer require guoguo/tpflow
*  复制assets/work到项目资源目录
*  修改src/tpflow/config/common.php的配置文件
*  博客教程 [官方博客](https://www.cojz8.com/ "官方博客")
*  视频免费教程 [B站查看](https://www.bilibili.com/video/BV1VZ4y1A71Q)
*   开源不易，知识付费  详细教程请购买文档 [购买](https://gadmin8.com/index/product.html)

## ⌛版权及严正声明

~~~
设计器、源代码标注有版权的位置，未经许可，严禁删除及修改，违者将承担法律侵权责任！
坚决打击网络诈骗行为，严禁将本插件集成在任何违法违规的程序上。
~~~


## ✨ 鸣谢  Thanks

- 感谢 [JetBrains](https://www.jetbrains.com) 提供生产力巨高的 `PHPStorm`和`WebStorm`
> 排名不分先后

- [top-think/think](https://github.com/top-think/think)
- [jsplumb](https://jsplumbtoolkit.com)
- [Layui](https://www.layui.com)
- [luhu 专业导航](https://www.luhu.co)
- [CCflow 国内最优秀的开源流程引擎](https://gitee.com/opencc/ccflow?_from=gitee_search)
- [leipi 优秀的表单设计器，流程引擎](http://www.leipi.org.cn)
- [woocms wooadmin 便捷高效的快速建站,后台开发框架](https://wooadmin.cn/)



## ㊙️ 链接传送门

---

> 官方博客：https://www.cojz8.com/

> 演示站点：http://tpflow.cojz8.com/

> 工作流手册：https://gadmin8.com/index/product.html  【付费】

> 视频教程：https://gadmin8.com/index/product.html  【付费】

---


## ©️ 版权信息

Tpflow 遵循 MIT 开源协议发布，并提供免费使用。

使用本项目必须保留所有版权信息。

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2018-2022 by Tpflow (http://cojz8.com)

All rights reserved。

~~~
对您有帮助的话，你可以在下方赞助我们，让我们更好的维护开发，谢谢！
特别声明：坚决打击网络诈骗行为，严禁将本插件集成在任何违法违规的程序上。
~~~

如果对您有帮助，您可以点右上角 💘Star💘支持